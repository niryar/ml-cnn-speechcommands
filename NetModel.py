
import torch.nn as nt
import torch.nn.functional as fu

class NetModel(nt.Module):
    def __init__(self):
        super(NetModel, self).__init__()
        self.layer1 = nt.Sequential(
            nt.Conv2d(1, 10, kernel_size=5, stride=1, padding=2),
            nt.ReLU(),
            nt.MaxPool2d(kernel_size=2, stride=2))
        self.layer2 = nt.Sequential(
            nt.Conv2d(10, 20, kernel_size=5, stride=1, padding=2),
            nt.ReLU(),
            nt.MaxPool2d(kernel_size=2, stride=2))
        self.layer3 = nt.Sequential(
            nt.Conv2d(20, 20, kernel_size=5, stride=1, padding=2),
            nt.ReLU(),
            nt.MaxPool2d(kernel_size=2, stride=2))
        self.drop_out = nt.Dropout()

        self.fc1 = nt.Linear(4800, 900)
        self.fc2 = nt.Linear(900, 100)
        self.fc3 = nt.Linear(100, 30)

    def forward(self, x):
        out = self.layer1(x)
        out = self.layer2(out)
        out = self.layer3(out)
        out = fu.relu(out)
        out = out.reshape(out.size(0), -1)
        out = self.drop_out(out)
        out = self.fc1(out)
        out = self.fc2(out)
        out = self.fc3(out)
        return out
