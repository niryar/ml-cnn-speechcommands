from torch import optim
from torch.nn import CrossEntropyLoss
from torch.nn.functional import nll_loss

from NetModel import NetModel
from gcommand_loader import GCommandLoader
import torch

LR = 0.001
EPOCHES = 7


def test_name_list(spects):
    lst = []
    for i in range(len(spects)):
        name = spects[i][0]
        firstCharIndex = name.rfind('\\') + 1
        lst.append(name[firstCharIndex:])
    return lst


def test_without_write(model, valid_loader):
    model.eval()
    with torch.no_grad():
        correct = 0
        total = 0
        for images, labels in valid_loader:
            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()

        print('Test Accuracy of the model on the : {} %'.format((correct / total) * 100))


def test(model, test, names):
    model.eval()
    f = open("test_y", "w")
    with torch.no_grad():
        correct = 0
        total = 0
        p_list = []
        for images, labels in test:
            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            p_list.extend(predicted.tolist())
    for i in range(len(names)):
        f.write("{0}, {1}.\n".format(names[i], p_list[i]))
    f.close()


def train(model, train_loader, optimizer, criterion):
    model.train()
    total_step = len(train_loader)
    lst_loss = []
    lst_avg = []
    for e in range(EPOCHES):
        for index, (images, labels) in enumerate(train_loader):
            # forward
            outputs = model(images)
            loss = criterion(outputs, labels)
            lst_loss.append(loss.item())

            # backprop
            # perform Adam optimisation
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            # accuracy
            total = labels.size(0)
            _, predicted = torch.max(outputs.data, 1)
            correct = (predicted == labels).sum().item()
            lst_avg.append(correct / total)

            if (index + 1) % 100 == 0:
                print('Epoch [{}/{}], Step [{}/{}], Loss: {:.4f}, Accuracy: {:.2f}%'
                      .format(e + 1, EPOCHES, index + 1, total_step, loss.item(),
                              (correct / total) * 100))
                # test_without_write(model,valid_loader)


def main():
    dataset_train = GCommandLoader('./data/train')
    dataset_validation = GCommandLoader('./data/valid')
    dataset_test = GCommandLoader('./data/test')

    train_loader = torch.utils.data.DataLoader(
        dataset_train,
        batch_size=100, shuffle=True,
        num_workers=20, pin_memory=True, sampler=None)

    valid_loader = torch.utils.data.DataLoader(
        dataset_validation, batch_size=100, shuffle=None,
        num_workers=20, pin_memory=True, sampler=None)

    test_loader = torch.utils.data.DataLoader(
        dataset_test, batch_size=100, shuffle=None,
        num_workers=20, pin_memory=True, sampler=None)

    model = NetModel()
    criterion = CrossEntropyLoss()
    optimizer = optim.Adam(model.parameters(), lr=LR)

    train(model, train_loader, optimizer, criterion)
    name_test = []
    name_test = test_name_list(dataset_test.spects)
    test(model, test_loader, name_test)
    # test_without_write(model,valid_loader)


if __name__ == '__main__':
    main()
